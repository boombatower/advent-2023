use std::collections::HashMap;
use std::fs::read_to_string;
use std::u32;

use regex::Regex;

struct Extractor {
    first: Regex,
    last: Regex,
    map: HashMap<String, u32>,
}

fn main() {
    const REGEX_BASE: &str = r"\d|one|two|three|four|five|six|seven|eight|nine";
    let extractor = Extractor {
        first: Regex::new(REGEX_BASE).unwrap(),
        last: Regex::new(format!(".*({REGEX_BASE})").as_str()).unwrap(),
        map: HashMap::from([
            ("one".into(), 1),
            ("two".into(), 2),
            ("three".into(), 3),
            ("four".into(), 4),
            ("five".into(), 5),
            ("six".into(), 6),
            ("seven".into(), 7),
            ("eight".into(), 8),
            ("nine".into(), 9),
        ]),
    };

    let mut sum = 0;
    for line in read_to_string("data/calibration.txt").unwrap().lines() {
        sum += match extractor.extract_value(line) {
            None => {
                eprintln!("Skipping line: {}", line);
                continue
            },
            Some(value) => value,
        };
    }

    println!("{sum}");
}

impl Extractor {
    fn extract_value(&self, line: &str) -> Option<u32> {
        let first = self.digit_extract(self.first.find(line)?.as_str());
        let last = self.digit_extract(self.last.captures(line)?.get(1)?.as_str());

        Some(first? * 10 + last?)
    }

    fn digit_extract(&self, input: &str) -> Option<u32> {
        if let Ok(digit) = input.parse() {
            Some(digit)
        } else {
            Some(self.map.get(input)?.to_owned())
        }
    }
}
