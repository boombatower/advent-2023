use std::fs::read_to_string;
use std::u32;

fn main() {
    let mut sum = 0;
    for line in read_to_string("data/calibration.txt").unwrap().lines() {
        sum += match extract_value(line) {
            None => {
                eprintln!("Skipping line: {}", line);
                continue
            },
            Some(value) => value,
        };
    }

    println!("{sum}");
}

fn extract_value(line: &str) -> Option<u32> {
    let mut first = None;
    let mut last = None;

    for character in line.chars() {
        if let Some(digit) = character.to_digit(10) {
            if first.is_none() {
                first = Some(digit);
            }
            last = Some(digit);
        }
    }

    Some(first? * 10 + last?)
}
